from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin 
from django.contrib.auth.base_user import BaseUserManager

class UsuarioManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password=None, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError('Se debe definir una matricula.')
        matricula = self.model.normalize_username(username)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser debe tener su valor is_superuser=True')

        return self._create_user(username, password, **extra_fields)

class Usuario(AbstractBaseUser, PermissionsMixin):
    #username = None
    username = models.CharField(max_length=10, blank=False, primary_key=True, unique=True)
    nombre = models.CharField(max_length=20, blank=False)
    ap_pat = models.CharField(max_length=20, blank=True, null=True)
    ap_mat = models.CharField(max_length=20, blank=True, null=True)
    password = models.CharField(max_length=50)
    correo = models.EmailField(blank=True, null=True)
    is_student = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_parent = models.BooleanField(default=False)
    class Meta:
        permissions = (
            ("is_alumno", "Es Alumno"),
            ("is_profesor", "Es Profesor"),
            ("is_padre", "Es Tutor"),
        )

    objects = UsuarioManager()
    
    USERNAME_FIELD = 'username'
    
    REQUIRED_FIELDS = ['nombre']

# Create your models here.
class Alumno(models.Model):
    id_alumno = models.CharField(max_length=30, primary_key=True, default=None)
    profile = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    grupo = models.ForeignKey('proyecto_eneit.Grupo', null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        db_table='Alumnos'
    
    def __str__(self):
        return str(self.id_alumno)

class Padre(models.Model):
    id_padre = models.CharField(max_length=30, primary_key=True, default=None)
    profile = models.ForeignKey(Usuario, blank=True, null=True, on_delete=models.CASCADE)
    hijos = models.ManyToManyField(Alumno, blank=True)
    rol_tutor = models.CharField(max_length=50, null=True)

    class Meta:
        db_table='Padres'

    def __str__(self):
        return str(self.id_padre)

class Profesor(models.Model):
    id_profesor = models.CharField(max_length=30, primary_key=True, default=None)
    profile = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    
    class Meta:
        db_table='Profesores'

    def __str__(self):
        return "Prof. "+str(self.profile.nombre)+" "+str(self.profile.ap_pat)+" "+str(self.profile.ap_mat)
    
