from django.db import models
from accounts.models import Alumno, Padre, Profesor, Usuario

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.core.exceptions import ValidationError

class Signup_User(forms.ModelForm):
    def clean_matricula(self):
        username = self.cleaned_data.get('username').upper()
        r = Usuario.objects.filter(username=username)
        if r.count():
            raise ValidationError("Matricula ya existente")
        return username

    class Meta:
        model = Usuario
        fields = ['username', 'correo', 'password', 'nombre', 'ap_pat', 'ap_mat', 'is_student', 'is_parent', 'is_teacher']
        labels ={
            'username': 'Matricula',
            'nombre': 'Nombre',
            'ap_pat': 'Apellido Paterno',
            'ap_mat': 'Apellido Materno',
            'is_student': 'Estudiante',
            'is_parent': 'Padre, Madre o Tutor',
            'is_teacher': 'Profesor(a)',
        }
        widgets = {
            'password': forms.PasswordInput(),
        }

    def clean_password(self):
        password = self.cleaned_data.get('password')
        return password
    
    def save(self, commit=True):
        user = super(Signup_User, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ['username', 'correo', 'nombre', 'ap_pat', 'ap_mat', 'is_student', 'is_parent', 'is_teacher']
        labels = {
            'username': 'Matricula',
            'nombre': 'Nombre',
            'ap_pat': 'Apellido Paterno',
            'ap_mat': 'Apellido Materno',
            'is_student': 'Estudiante',
            'is_parent': 'Padre, Madre o Tutor',
            'is_teacher': 'Profesor(a)',
        }

class AlumnoForm(forms.ModelForm):
    class Meta:
        model = Alumno
        fields = ['grupo']

class PadreForm(forms.ModelForm):
    class Meta:
        model = Padre
        fields = ['hijos', 'rol_tutor']

class ProfesorForm(forms.ModelForm):
    class Meta:
        model = Profesor
        fields = []



