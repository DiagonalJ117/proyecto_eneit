from django.urls import path, include
from accounts import views


app_name = 'accounts'

urlpatterns = [
    

    path('login/', views.login_view, name="login"),
    path('signup/', views.signup_view, name="signup"),
    path('<str:username>', views.perfil, name="account"),
    path('update/', views.editar, name="update"),
    path('update_user/', views.editar_usuario, name="account_update"),
    path('lista_usuarios/', views.lista_usuarios, name="user_list"),
    #path('staff_signup/', views.staffsignup_view, name="staff_signup"),
    path('logout/', views.logout_view, name="logout"),
    path('delete/', views.eliminar, name="delete_user"),

    #test stuff
    
    #path('update/', views.account_update, name="account_update")
]
