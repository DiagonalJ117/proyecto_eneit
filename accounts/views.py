from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from uuid import UUID
from accounts.models import Alumno, Padre, Profesor, Usuario
from accounts.forms import Signup_User , UserProfileForm, AlumnoForm, PadreForm, ProfesorForm #,Signup_Alumno, Signup_Padre, Signup_Profesor

# Create your views here.

#los diferentes tipos de perfiles solo iran en una view de perfil. para propositos de demo, iran en 3 diferentes.
#alumnos

def signup_view(request):
    if request.method=='POST':
        form = Signup_User(request.POST)
        if form.is_valid():
            #permission = Permission.objects.get(name='is_staff')
            
            new_user = form.save(commit=False)
            
            
            
            new_user.save()
            
            if new_user.is_student:
               # form2 = Signup_Alumno()
               Alumno.objects.create(profile=new_user, id_alumno = new_user.username)
               
            if new_user.is_teacher:
                Profesor.objects.create(profile=new_user, id_profesor = new_user.username)
                
            if new_user.is_parent:
                Padre.objects.create(profile=new_user, id_padre = new_user.username)
                
            #user.set_permissions.add(permission)
            #login(request, user)
            messages.success(request, 'Cuenta creada!')
            return redirect('accounts:signup') #HttpResponse('Success!')
    else:
        
        form = Signup_User()

    return render(request, 'signup.html', {'form': form})

#una login view para todos
def login_view(request):
    if request.method=='POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                #return redirect('accounts:account', request.user.username)
                return redirect('inicio')
        else:
            messages.error(request, "Nombre de Usuario o contraseña incorrecta.")
    else:
        form = AuthenticationForm()

    return render(request, 'login.html', {'form': form})

def logout_view(request):
    if request.method == 'POST':
        logout(request)
        #return redirect('accounts:login')
        return redirect('inicio')
        
    else:
        return HttpResponse('Not a POST request')

def lista_usuarios(request):
    users = Usuario.objects.all()
    return render(request, 'lista_usuarios.html', {'users': users})

def perfil(request, username):
    user = Usuario.objects.get(username=username)
    return render(request, 'perfil.html', {'user':user})

def editar(request):
    if request.method=="POST":
        user = request.user
        form = UserProfileForm(data=request.POST, instance=request.user)
        update = form.save(commit=False)
        
        update.save()
        return redirect('accounts:account', request.user)
    else:
        form=UserProfileForm(instance=request.user)
   
    return render(request, 'editar_perfil.html', {'form': form, 'user': user})

def editar_usuario(request):
    
    
    if request.method=="POST":
        user = Usuario.objects.get(username=request.POST.get('username'))
        form = UserProfileForm(data=request.POST, instance=user)
        update = form.save(commit=False)
        
        update.save()
        return redirect('accounts:user_list')
    else:
        user = Usuario.objects.get(username=request.GET.get('username'))
        form=UserProfileForm(instance=user)
   
    return render(request, 'editar_perfil.html', {'form': form, 'user':user})

def eliminar(request):
    username = request.POST.get('username')
    user = Usuario.objects.get(username=username)
    if request.method=="POST":
        
        user.delete()
        messages.success(request,'Usuario eliminado!')
    return redirect('accounts:user_list')

