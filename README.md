# Gedulle

<img src="https://gitlab.com/FWC5757/proyecto_eneit/raw/master/static/gedulle_logo.png" align="center">

Sistema gestor de procesos académicos para primarias y secundarias

* Mantiene control de asistencia, calificaciones y reportes

* Control de actividades en clase y tareas

* Emisión de copia de reportes de conducta para padres.

* Anuncios de reuniones para padres o tutores.

* Calendario Escolar

## Backend

Backend en Django 2.2
<img src="https://static.djangoproject.com/img/logos/django-logo-positive.png" width="150" align="center">

## Frontend

Frontend con Bootstrap 4 

<img src="https://cdn.worldvectorlogo.com/logos/bootstrap-4.svg" width="150" align="center">