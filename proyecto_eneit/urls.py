"""proyecto_eneit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from proyecto_eneit import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.inicio, name="inicio"),
    path('accounts/', include('accounts.urls')),
    #path('tareas/', views.tareas, name="tareas"),
    path('crear_horario/', views.crear_horario, name="crear_horario"),
    path('calendario/', views.calendario_escolar, name="calendario"),
    path('editar_padre/', views.editar_padre, name="editar_padre"),
    path('editar_alumno/', views.editar_alumno, name="editar_alumno"),
    path('editar_profesor/', views.editar_profesor, name="editar_profesor"),
    path('editar_horario/', views.editar_horario, name="editar_horario"),
    path('editar_materia/', views.editar_materia, name="editar_materia"),

    path('horarios/', views.horarios, name="horarios"),
    path('mi_horario/', views.mi_horario, name="mi_horario"),
    
    
    path('eliminar_horario/', views.eliminar_horario, name="eliminar_horario"),
    path('eliminar_materia/', views.eliminar_materia, name="eliminar_materia"),
    path('eliminar_grupo/', views.eliminar_grupo, name="eliminar_grupo"),
    #grupos
     path('editar_grupo/<str:id_grupo>', views.editar_grupo, name="editar_grupo"),
    path('grupos/', views.lista_grupos, name="lista_grupos"),
    path('detalle_grupo/<str:id_grupo>', views.detalle_grupo, name="detalle_grupo"),
    path('crear_grupo/', views.crear_grupo, name="crear_grupo"),
   
    
    #alumnos
    path('alumnos/', views.lista_alumnos, name="lista_alumnos"),
    
    #padres
    path('padres/', views.lista_padres, name="lista_padres"),
    
    #profesores
    path('profesores/', views.lista_profesores, name="lista_profesores"),
    
    #materias
    path('materias/', views.lista_materias, name="lista_materias"),
    path('crear_materia/', views.crear_materia, name="crear_materia"),
    
    
    path('boleta/', views.boleta, name="boleta"),
    path('califs/', views.calif_parciales, name="califs")
]
