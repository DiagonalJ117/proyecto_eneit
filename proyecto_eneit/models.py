import os
from datetime import datetime, timedelta
from django.db import models
from django import forms

class Materia(models.Model):
    id_materia = models.CharField(max_length=15, primary_key=True)
    nombre = models.CharField(max_length=25, null=True)

    def __str__(self):
        return str(self.nombre)

class Unidad(models.Model):
    id_unidad= models.CharField(max_length=12, primary_key=True)
    materia = models.ForeignKey(Materia, default=None, blank=True, null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, null=True)

class Tema(models.Model):
    id_tema = models.CharField(max_length=12, primary_key=True)
    unidad = models.ForeignKey(Unidad, default=None, blank=True, null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, null=True)

class Grupo(models.Model):
    id_grupo = models.CharField(max_length=10, primary_key=True)
    cantidad_alumnos = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.id_grupo)


class Horario(models.Model):
    dia_hora = models.DateTimeField(null=True)
    grupo = models.ForeignKey(Grupo, default=None, blank=True, null=True, on_delete=models.CASCADE)
    profesor = models.ForeignKey('accounts.Profesor', default=None, blank=True, null=True, on_delete=models.CASCADE)
    materia = models.ForeignKey(Materia, default=None, blank=True, null=True, on_delete=models.CASCADE)


class Reunion(models.Model):
    id_reunion = models.IntegerField(primary_key=True)
    fecha_hora = models.DateTimeField()
    lugar = models.CharField(max_length=30, null=True)
    asunto = models.CharField(max_length=150, null=True)
    padres_invitados = models.ManyToManyField('accounts.Padre')

class Actividad(models.Model):
    id_actividad=models.IntegerField(primary_key=True)
    grupo= models.ForeignKey(Grupo, default=None, blank=True, null=True, on_delete=models.CASCADE)
    materia = models.ForeignKey(Materia, default=None, blank=True, null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, blank=False, null=True)
    instrucciones = models.CharField(max_length=500, blank=False, null=True)
    ponderacion = models.FloatField()
    calificacion = models.FloatField()
    fecha_hora_ent= models.DateTimeField()


