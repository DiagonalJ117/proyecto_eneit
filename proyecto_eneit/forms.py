from django import forms
from django.db import models
from proyecto_eneit.models import Grupo, Materia, Horario
from django.core.exceptions import ValidationError
from django.forms import DateTimeField


class createGrupoForm(forms.ModelForm):
    def clean(self):
        id_grupo = self.cleaned_data['id_grupo'].upper()
        r = Grupo.objects.filter(id_grupo=id_grupo)
        if r.count():
            raise ValidationError("Grupo ya existente")
        return id_grupo
    class Meta:
        model = Grupo
        fields = ['id_grupo']

class editarGrupoForm(forms.ModelForm):
    class Meta:
        model = Grupo
        fields = ['id_grupo']


class createMateriaForm(forms.ModelForm):
    def clean_idMateria(self):
        id_materia = self.cleaned_data['id_materia'].upper()
        r = Materia.objects.filter(id_materia=id_materia)
        if r.count():
            raise ValidationError("Materia ya existente")
        return id_materia
    class Meta:
        model = Materia
        fields = ['id_materia', 'nombre']

class editarMateriaForm(forms.ModelForm):
    class Meta:
        model = Materia
        fields = ['id_materia', 'nombre']


class createHorarioForm(forms.ModelForm):
    dia_hora = DateTimeField(widget=forms.widgets.DateTimeInput(format="%d %b %Y %H:%M:%S %Z"))
    class Meta:
        model = Horario
        fields = ['dia_hora', 'grupo', 'profesor', 'materia']

class editarHorarioForm(forms.ModelForm):
    widget = forms.DateTimeInput(attrs={
        'class': 'form-control datetimepicker-input',
        'data-target': '#datetimepicker1'
    })
    class Meta:
        model = Horario
        fields = ['dia_hora', 'grupo', 'profesor', 'materia']


