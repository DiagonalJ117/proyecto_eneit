import datetime
from datetime import date, timedelta
from django.db.models import Count
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth import login, logout
from accounts.models import Alumno, Padre, Profesor
from accounts.forms import AlumnoForm, PadreForm, ProfesorForm
from .models import Grupo, Materia, Horario
from proyecto_eneit.forms import createGrupoForm, createMateriaForm, createHorarioForm, editarGrupoForm, editarMateriaForm, editarHorarioForm
from django.contrib.auth.decorators import login_required, permission_required

def inicio(request):
    grupos = Grupo.objects.all()
    alumnos = Alumno.objects.all()
    profesores = Profesor.objects.all()
    padres = Padre.objects.all()



    return render(request, 'home.html')


def mi_horario(request):
    return render(request, 'mi_horario.html')

#views del alumno


def lista_alumnos(request):
    alumnos = Alumno.objects.all()
    return render(request, 'lista_alumnos.html', {'alumnos':alumnos})

def editar_alumno(request):
    
    if request.method=="POST":
        alumno = Alumno.objects.get(id_alumno=request.POST.get('id_alumno'))
        form = AlumnoForm(data=request.POST, instance=alumno)
        update = form.save(commit=False)
        
        update.save()
        return redirect('lista_alumnos')
    else:
        alumno = Alumno.objects.get(id_alumno=request.GET.get('id_alumno'))
        form=AlumnoForm(instance=alumno)
   
    return render(request, 'editar_alumno.html', {'form': form, 'alumno':alumno})

def lista_profesores(request):
    profesores = Profesor.objects.all()
    return render(request, 'lista_profesores.html', {'profesores':profesores})

def editar_profesor(request):
    profesor = Profesor.objects.get(id_profesor=request.GET.get('id_profesor'))
    if request.method=="POST":
        form = ProfesorForm(data=request.POST, instance=profesor)
        update = form.save(commit=False)
        
        update.save()
        return redirect('lista_padres')
    else:
        form=ProfesorForm(instance=profesor)
   
    return render(request, 'editar_profesor.html', {'form': form})

def lista_padres(request):
    padres = Padre.objects.all()
    return render(request, 'lista_padres.html', {'padres':padres}) 

def editar_padre(request):
    if request.method=="POST":
        padre = Padre.objects.get(id_padre=request.POST.get('id_padre'))
        form = PadreForm(data=request.POST, instance=padre)
        update = form.save(commit=False)
        
        update.save()
        return redirect('lista_padres')
    else:
        padre = Padre.objects.get(id_padre=request.GET.get('id_padre'))
        form=PadreForm(instance=padre)
   
    return render(request, 'editar_padre.html', {'form': form, 'padre':padre})

def lista_grupos(request):
    grupos = Grupo.objects.all()
    return render(request, 'lista_grupos.html', {'grupos':grupos})

def horarios(request):
    horarios = Horario.objects.all().order_by('grupo')
    return render(request, "lista_horarios.html", {'horarios': horarios})

def crear_grupo(request):
    if request.method=="POST":
        form = createGrupoForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Grupo creado!')
            return redirect('inicio')

    else:
        form = createGrupoForm()

    return render(request, 'crear_grupo.html', {'form': form})

def detalle_grupo(request, id_grupo):
    grupo = Grupo.objects.get(id_grupo=id_grupo)
    horario = Horario.objects.filter(grupo = id_grupo)
    alumnos = Alumno.objects.filter(grupo = id_grupo)
    

    return render(request, 'grupo.html', {'grupo': grupo, 'horario': horario, 'alumnos': alumnos})

def calendario_escolar(request):
    return render(request, 'calendario.html')

def editar_grupo(request, id_grupo):
    grupo = Grupo.objects.get(id_grupo=id_grupo) 
    if request.method=="POST":
        
        form = editarGrupoForm(request.POST, instance = grupo  )
        if form.is_valid():
            
            form.save()
            messages.success(request, 'Grupo actualizado!')
            return redirect('lista_grupos')

    else:
        
        form = editarGrupoForm( instance=grupo)

    return render(request, 'editar_grupo.html', {'form': form, 'grupo':grupo})

    

def eliminar_grupo(request):
    grupo = Grupo.objects.get(id_grupo=request.POST.get('id_grupo')) 
    if request.method =="POST":
        grupo.delete()
        messages.info(request,'Grupo Borrado!')
    return redirect('lista_grupos')

def lista_materias(request):
    materias = Materia.objects.all()
    return render(request, 'materias.html', {'materias':materias})

def crear_materia(request):
    if request.method=="POST":
        form = createMateriaForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Materia creada!')
            return redirect('lista_materias')

    else:
        form = createMateriaForm()

    return render(request, 'crear_materia.html', {'form': form})
    

def editar_materia(request):
    materia = Materia.objects.get(id_materia=request.GET.get('id_materia')) 
    if request.method=="POST":
        form = editarMateriaForm(request.POST, instance = materia )
        if form.is_valid():
            form.save()
            messages.success(request, 'Materia actualizada!')
            return redirect('inicio')

    else:
        form = editarMateriaForm()

    return render(request, 'editar_grupo.html', {'form': form})
    

def eliminar_materia(request):
    materia= Materia.objects.get(id_materia=request.POST.get('id_materia')) 
    if request.method =="POST":
        materia.delete()
        messages.info(request,'Materia borrada!')
    return redirect('lista_materias')

def crear_horario(request):
    if request.method=="POST":
        form = createHorarioForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Registro de horario creado!')
            return redirect('inicio')

    else:
        form = createHorarioForm()

    return render(request, 'crear_horario.html', {'form': form})
    

def editar_horario(request):
    horario = Horario.objects.get(id_horario=request.GET.get('id_horario')) 
    if request.method=="POST":
        form = editarHorarioForm(request.POST, instance = horario )
        if form.is_valid():
            form.save()
            messages.success(request, 'Registro de horario actualizado!')
            return redirect('inicio')

    else:
        form = editarHorarioForm()

    return render(request, 'editar_horario.html', {'form': form})
    

def eliminar_horario(request):
    horario_id = request.POST.get('id_horario')
    if request.method == "POST":
        horario = Horario.objects.get(id=id) 
        if request.method =="GET":
            horario.delete()
            messages.info(request,'Registro de horario borrado!')
    return redirect('horarios')
    
    



def boleta(request):
    return render(request, 'boleta_calificaciones.html')

def calif_parciales(request):
    return render(request, 'calificaciones_materia.html')